import numpy as np # 
from sklearn.metrics import matthews_corrcoef
from sklearn.metrics import roc_auc_score
from sklearn import svm
from sklearn.model_selection import GridSearchCV
import re
from itertools import *
import itertools
from multiprocessing import Pool
from functools import partial

filename = "/home/andres/Scene.txt"#name of file to load

#open file; readlines() is used to read all lines
with open(filename) as f:
	data = f.readlines()

#for each line we split using comma as pattern, besides we erase comments (lines with #)
data = [map(float, re.split(",",i[:-1])) for i in data  if i.startswith("#")==False]

#the first part of the file are features stored in x, the second part are labels y

datalen = len(data)/2
x = np.asarray(data[:datalen])
y = np.asarray(data[datalen:])

#This function performs grid search optimization
#inputs: 
#features: employed data
#labels: outputs
#par: Grid ( each row is a combination (C,gamma)  )
#weights: for each sample we assigned a weight
#k_fold: number of folds in cross validation
def cross_val_func(add,par):
	
	#additional parameters should be in a list 
	features=add[0]
	labels=add[1]
	weights=add[2]
	k_fold=add[3]
	
	samples = np.tile(range(k_fold),features.shape[0])[:features.shape[0]]# this vector is used for stratified cross validation
	AUC = np.zeros([k_fold])#matrix with AUC values for each fold and for each class
	
	for m in range(k_fold):#for each fold in cross validation
		
		svc = svm.SVC(C=par[0],gamma=par[1])#declaring svm
		svc.fit(features[samples!=m],labels[samples!=m],weights[samples!=m])#training classifier
		#svc.fit(features[samples!=m],labels[samples!=m])#training classifier
		
		pred = svc.predict(features[samples==m])#prediction of test fold
		AUC[m] = matthews_corrcoef(labels[samples==m], pred)#computing area under roc curve AUC
		
	
	#At final we return mean of folds in cross validation 
	return(np.mean(AUC))
	

#this function is employed for binary relevance (one vs all)
#inputs 
#x: features
#y: matrix if labels (1 positive) and (0 negative)
#k_fold is the number of folds in cross validation
def binary_relevance(x,y,k_fold):
	
	classes = y.shape[1]#classes is the number of cols of y
	samples = np.tile(range(k_fold),x.shape[0])[:x.shape[0]]# this vector is used for stratified cross validation
	#if k_fold=5 then samples=[1,2,3,4,5,1,2,3,4,5,1,2,3,4,5,...]
	
	#Grid_search optimization
	gam=2.**np.arange(-5,3)
	C=10.**np.arange(-1,3)
	par = np.array(tuple(itertools.product(*[C,gam])))
	#weights = np.sum(y,1)
	weights = np.repeat(1,len(y))
	
	AUC = np.zeros([k_fold,classes])#matrix with AUC values for each fold and for each class
	for i in range(classes):#for each class
		
		for k in range(k_fold):#for each test fold
			
			#additional parameters in the function cross_val_func
			features = x[samples!=k]
			labels = y[samples!=k,i]
			w = weights[samples!=k]
			k_fold = 5
			
			# Grid search using parallelization
			cores = 3 # number of jobs
			pool = Pool(processes=cores)
			auc = pool.map( partial( cross_val_func, [features,labels,w,k_fold] ) , par)#obtaining performance for each hyperparameter combination
			
			opt_par = par[np.argmax(auc)]#obtaining the best combination
			
			svc = svm.SVC(C = opt_par[0], gamma = opt_par[1])#declaring svm
			svc.fit(x[samples!=k], y[samples!=k,i], weights[samples!=k])#hyperparameter optimization and training classifier
			#svc.fit(x[samples!=k], y[samples!=k,i])
			pred = svc.predict(x[samples==k])#prediction of test fold
			AUC[k,i] = matthews_corrcoef(y[samples==k,i], pred)#computing area under roc curve AUC
			
		
	
	return(AUC)
	

AUCbr = binary_relevance(x,y,5)

#powerset function is used to construct the power-set for classes
#inputs: matrix with labels
#outputs: list with all possible groups among classes
#example for 3 classes:  [[1],[2],[3],[1,2],[1,3],[2,3],[1,2,3]]
def powerset(labels):
	s = list(range(labels.shape[1]))
	ps = list( chain.from_iterable(combinations(s, r) for r in range(len(s)+1)) )[1:]
	return [list(psi) for psi in ps]

groups = powerset(y)
#constructing a binary problem for each member of power set
lp = np.transpose( np.asarray([ np.where(np.sum(y[:,k],1)==len(k),1,0) for k in groups ]) )
it = np.array(np.where(np.sum(lp,0)!=0)).reshape((-1,))

#power-set method
AUCps = binary_relevance(x,lp[:,it],5)


def pairwise(labels):
	s = list(combinations(range(labels.shape[1]), 2))
	return [list(i) for i in s]

groups = pairwise(y)
#constructing a binary problem for each possible pair of classes
pc = np.transpose( np.asarray([ np.where(np.sum(y[:,k],1)==len(k),1,0) for k in groups ]) )
pc = np.concatenate([pc,1.-pc],1)
it = np.array( np.where(   np.logical_and(np.sum(pc,0)!=0, np.sum(pc,0)!=len(pc))   ) ).reshape((-1,))

AUCpc = binary_relevance(x,pc[:,it],5)
