
from numpy import matrix,zeros,size
from numpy.linalg import det
A=matrix([[4, -3, 1],[2, -1, 5],[5, 1, -4]])
MC=matrix(zeros((3,3))) # Matriz de cofactores
idx=matrix(range(3))
for i in range(size(A,0)):
    for j in range(size(A,1)):
        fidx=idx[idx!=i]
        cidx=idx[idx!=j]
        cof=A[[[fidx[0,0]],[fidx[0,1]]],cidx]
        MC[i,j]=pow(-1,i+j)*det(cof)

MADJ=MC.transpose() # Matriz adjunta 
print (MC)
print (MADJ)
