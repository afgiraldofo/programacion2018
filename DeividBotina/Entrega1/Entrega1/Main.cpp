#include <iostream>
#include <fstream>
#include <stdlib.h>
#include <math.h>
#include <stdio.h>
#include <string>
#include <vector>
#include <algorithm>


using namespace std;

// PROTOTIPOS


/*
Presentado por: DEIVID JOHAN BOTINA MONSALVE

*/

void leerTXT(char*nombre, vector<float>* londa, vector<float>* magnitud);


int main(void)
{
	vector<float> londa, datosB, datosN, firma;
	leerTXT("blanco.txt", &londa, &datosB);
	leerTXT("negro.txt", &londa, &datosN);
	leerTXT("firma.txt", &londa, &firma);

	//FORMULA DE CALIBRACI�N: valor - negro / blanco - negro;

	vector<float> FirmaCalib;
	FirmaCalib.resize(datosN.size());

	for (int i = 0; i < FirmaCalib.size(); i++) {
		FirmaCalib[i] = (firma[i] - datosN[i]) / (datosB[i]-datosN[i]);
	}

	getchar();
	return 0;
}

//FUNCI�N PARA leer las firmas
void leerTXT(char*nombre, vector<float>* londa, vector<float>* magnitud) {
	
	londa->clear();//Borrar si tiene datos

	ifstream documento;
	string s;
	bool empezar = false;

	//Abrir documento
	documento.open(nombre);
	//�Abri� el documento?
	if (!documento.is_open()) {
		cout << "Error. no se ha podido abrir el archivo" << endl;
		return;
	}

	// Leemos el contenido del archivo linea a linea 
	while (getline(documento, s))
	{
		if (empezar) {
			replace(s.begin(), s.end(), ',', '.');//cambiamos comas por puntos
			//Primero encuentro en donde termina el primer n�mero
			size_t espacio = s.find_first_of('\t');
			char numer1[10], numer2[10];
			s.copy(numer1, espacio - 1, 0);//extraigo el primer n�mero
			s.copy(numer2, 10, espacio + 1);//extraigo el segundo n�mero
			londa->push_back(atof(numer1));//agregamos el valor de londa
			magnitud->push_back(atof(numer2));//agregamos el valor de magnitud
			//cout << " " << unsigned int(espacio) << endl;
		}

		//Cuando encuentre la siguiente sentencia puede empezar a tomar datos
		if (s == ">>>>>Begin Spectral Data<<<<<") {
			empezar = true;
		}
	}
}


